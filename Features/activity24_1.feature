@activity24_1
Feature: Testing with Tags

@SimpleAlert @SmokeTest
		Scenario: Test for Simple Alert
    Given User is on the page 
    When User clicks the Simple Alert button 
    Then Alert opens
    And Read the text from it and print it
    And Close the alert
    And Close Browser
    
@ConfirmAlert
	  Scenario: Test for Confirm Alert
	  Given User is on the page
	  When User clicks the Confirm Alert button
	  Then Alert Opens
	  And Read the text from it and print it
    And Close the alert with Cancel
    And Close Browser
    
@PromptAlert
Scenario Outline: Test for Prompt Alert
    Given User is on the page
    When User clicks the Prompt Alert button 
    Then Alert opens
    And Read the text from it and print it
    And Write "<Messages>" in it
    And Close the alert
    And Close Browser
	
Examples:
   |	  Messages      |
   | Yes, you are      |
   | This is automated |
   | Custom Message	|