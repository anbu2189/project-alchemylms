package session10;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

public class Activity10_1 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		WebDriver dr = new FirefoxDriver();
		Actions actions  = new Actions(dr);
		
		dr.get("https://www.training-support.net/selenium/input-events");
		String title = dr.getTitle();
		System.out.println("Title of the page is: " + title);
		
		WebElement cube = dr.findElement(By.id("side3"));

		//Left Click
		actions.click(cube);
		WebElement cubeval = dr.findElement(By.id("side4"));
		System.out.println("Left click text: " + cubeval.getText());
		Thread.sleep(2000);
		
		//double Click
		actions.doubleClick(cube).perform();
		cubeval =  dr.findElement(By.id("side2"));
		System.out.println("Double click text: " + cubeval.getText());
		Thread.sleep(2000);
		
		//Right Click
		actions.contextClick(cube).perform();
        cubeval = dr.findElement(By.id("side5"));
        System.out.println("Right Click text: " + cubeval.getText());
		Thread.sleep(2000);
				
		dr.close();
	}

}
