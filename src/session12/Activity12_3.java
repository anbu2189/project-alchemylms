package session12;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Activity12_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		WebDriver dr = new FirefoxDriver();
		WebDriverWait wait = new WebDriverWait(dr, 10);
        Actions builder = new Actions(dr);
		
		dr.get("https://www.training-support.net/selenium/popups");
		System.out.println("The title of the page is: " + dr.getTitle());
	
		//Find Sign in button
		WebElement button = dr.findElement(By.xpath("//button[contains(@class, 'orange')]"));
	
		//Hover over button
		builder.moveToElement(button).pause(Duration.ofSeconds(1)).build().perform();
		String tooltipText = button.getAttribute("data-tooltip");
		System.out.println("The tooltip Text is: " + tooltipText);
		
		//Click on the button
        button.click();
        
        //Wait for modal to appear
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("signInModal")));
        
        //Find username and pasword and fill in the details
        dr.findElement(By.id("username")).sendKeys("admin");
        dr.findElement(By.id("password")).sendKeys("password");
        dr.findElement(By.xpath("//button[text()='Log in']")).click();
        
      //Read the login message
        String message = dr.findElement(By.id("action-confirmation")).getText();
        System.out.println(message);
        
        //Close browser
        dr.close();
		
		
	}

}