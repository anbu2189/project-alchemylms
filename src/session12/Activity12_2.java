package session12;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity12_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriver dr = new FirefoxDriver();
		dr.get("https://www.training-support.net/selenium/nested-iframes");
		System.out.println("The title of the page is: " + dr.getTitle());
		
		 //Switch to first iFrame on the page
		dr.switchTo().frame(0);
		
		//Switch to first iFrame in that frame
		dr.switchTo().frame(0);
		
		//Get heading text in iFrame
		WebElement frame1 = dr.findElement(By.cssSelector("div.content"));
		System.out.println(frame1.getText());
		
		//Switch back to parent page
		dr.switchTo().defaultContent();
		
		//Switch to first iFrame on the page
        dr.switchTo().frame(0);

        //Switch to second iFrame in that frame
        dr.switchTo().frame(1);
        
        //Get heading text in iFrame
        WebElement frame2 = dr.findElement(By.cssSelector("div.content"));
        System.out.println(frame2.getText());

        //Close browser
        dr.close();
		
		}

}
