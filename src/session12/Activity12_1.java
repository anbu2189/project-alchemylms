package session12;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity12_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		WebDriver dr = new FirefoxDriver();
		dr.get("https://www.training-support.net/selenium/iframes");
		System.out.println("The Title of the page is: " + dr.getTitle());
		
		//Switch to first iFrame on the page
		dr.switchTo().frame(0);
		//Perform operation on the first frame
		WebElement heading1 = dr.findElement(By.xpath("/html/body/div[2]/div/div[2]/iframe"));
		System.out.println(heading1.getText());
	
		//Click button in iFrame 1
		WebElement button1 = dr.findElement(By.xpath("//button[@id='actionButton']"));
		System.out.println(button1.getText());
		System.out.println(button1.getCssValue("background-color"));
		button1.click();
		
		///Print New Button Info
		System.out.println(button1.getText());
		System.out.println(button1.getCssValue("background-color"));
		
		//Switch back to parent page
		dr.switchTo().defaultContent();
		
		//Switch to second iFrame on the page
		dr.switchTo().frame(1);
		
		 //Perform operation on the second frame
		WebElement heading2 = dr.findElement(By.xpath("/html/body/div[2]/div/div[3]/iframe"));
		System.out.println(heading2.getText());
		
		//Click button in iFrame 2
		WebElement button2 = dr.findElement(By.xpath("//*[@id=\"actionButton\"]"));
		System.out.println(button2.getText());
		System.out.println(button2.getCssValue("background-color"));
		button2.click();
		
		///Print New Button Info
		System.out.println(button2.getText());
		System.out.println(button2.getCssValue("background-color"));

		//Switch back to parent page
		dr.switchTo().defaultContent();
		
		 //Close browser
        dr.close();
	}

}
