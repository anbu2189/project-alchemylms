package cucumberTest;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = "Features",
    glue = {"stepdefinition"},
    tags = {"@activity23_1"},
    strict = true
)

public class ActivitiesRunner {

}
