package stepdefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class LoginTestSteps {
	WebDriver driver;
    WebDriverWait wait;
    
    @Given("^User is on Login page$")
    public void loginpage() {
    //setup instances
    driver=new FirefoxDriver();
    wait=new WebDriverWait(driver, 10);
    	
    //Open browser
    driver.get("https://www.training-support.net/selenium/login-form");
    
    }
    
	@When("^User enters \"(.*)\" and \"(.*)\"$")
	public void enterParameters(String username, String password) {
	    //Enter username from Feature file
	    driver.findElement(By.id("username")).sendKeys(username);

	    //Enter password from Feature file
	    driver.findElement(By.id("password")).sendKeys(password);

	    //Click Login
	    driver.findElement(By.tagName("button")).click();
	}

	@Then("^Read the page title and confirmation message$")
	public void readTitleandHeading() {
	
	wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.tagName("h2")));
	
	//Read the page title and heading
	String pageTitle = driver.getTitle();
	String pageHeading = driver.findElement(By.tagName("h2")).getText();
	
	//Print the page title and heading
	System.out.println("The PageTitle is :" + pageTitle);
	System.out.println("The PageHeading is :" + pageHeading);	
	}

	@And("^Close the Browser$")
	public void closeBrowser() {
		
	//Close the browser
	driver.close();	
	}
}
