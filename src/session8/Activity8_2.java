package session8;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Activity8_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		WebDriver dr = new FirefoxDriver();
		WebDriverWait wait = new WebDriverWait(dr, 10);
		
		dr.get("https://training-support.net/selenium/ajax");
		String title = dr.getTitle();
		System.out.println("Title of the page is: " + title);
		dr.findElement(By.xpath("//button[@class='ui violet button']")).click();
		
		//Wait for text to load
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.id("ajax-content"), "HELLO!"));
		
		//getText() and print it
		String ajaxtext = dr.findElement(By.id("ajax-content")).getText();
		System.out.println(ajaxtext);
		
		 //Wait for late text
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.id("ajax-content"), "I'm late!"));
        
        //Get late text and print it
        String lateText = dr.findElement(By.id("ajax-content")).getText();
        System.out.println(lateText);

        //Close browser
        dr.close();
    }
        
}
