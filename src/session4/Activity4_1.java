package session4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity4_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Create a new instance of the firefox Driver
		WebDriver driver = new FirefoxDriver();
		//Open the browser
		driver.get("https://www.training-support.net");
		
		//Check the title & print the page
		String title = driver.getTitle();
		System.out.println("Page title is: " + title);
		
		//absolute xpath
		driver.findElement(By.xpath("/html/body/div/div[1]/div/a")).click();
		
		//print new title of page
		 System.out.println("Heading is: " + driver.getTitle());
		
		 //Close the browser
        driver.close();
		
		
		

	}

}
