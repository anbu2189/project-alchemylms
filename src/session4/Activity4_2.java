package session4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity4_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Create a new instance of the firefox Driver
		WebDriver driver = new FirefoxDriver();
		//Open the browser
		driver.get("https://www.training-support.net/selenium/simple-form");
		
		//Find the title of page & print it
		String title = driver.getTitle();
		System.out.println("Page of the title is: " + title);
		
		 //Find the input fields and enter text
		driver.findElement(By.xpath("//input[@id='firstName']")).sendKeys("Anbarasan");
		driver.findElement(By.xpath("//input[@id='lastName']")).sendKeys("Alagarsamy");
		driver.findElement(By.xpath("//input[@type='email']")).sendKeys("anbuadonis2010@gmail.com");
		driver.findElement(By.xpath("//input[@id='number']")).sendKeys("0123456789");
		
		/*other way   
		WebElement firstName = driver.findElement(By.xpath("//input[@id = 'firstName']"));
        WebElement lastName = driver.findElement(By.xpath("//input[@id = 'lastName']"));
        firstName.sendKeys("Saahil");
        lastName.sendKeys("Sharma"); */
		
		 //Enter Message
	    driver.findElement(By.xpath("//textarea")).sendKeys("This is my message");
	    
	    //Submit
	    driver.findElement(By.xpath("//input[contains(@class, 'green')]")).click();
	    
	    //syntax By.xpath("//tagname[contains(@attribute, 'value')]")
		
		 //Close the browser
        driver.close();
        
	}

}
