package session4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity4_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Create a new instance of the Firefox driver
		WebDriver driver = new FirefoxDriver();
		 //Open the browser
		driver.get("https://www.training-support.net/selenium/target-practice");
		//Find the page title and print it
		String title = driver.getTitle();
		System.out.println("Title of the page is: " + title);
		
		//Find the third header on the page
		String thirdHeader = driver.findElement(By.xpath("//h3[@id='third-header']")).getText();
		System.out.println("Third header text is: " + thirdHeader);
		
		//Find the fifth header and get it's colour
		String fifthHeaderColor = driver.findElement(By.cssSelector("h5.ui.green.header")).getCssValue("Color");
		System.out.println("fifth header text is: " + fifthHeaderColor);
		
		//Find the violet button on the page and print all the class attribute values.
		  String violetButtonClasses = driver.findElement(By.xpath("//button[contains(text(), 'Violet')]")).getAttribute("class");
	      System.out.println("Violet button's classes are: " + violetButtonClasses);
		
		//Find the grey button on the page with Absolute XPath.
		String greyButton = driver.findElement(By.xpath("/html/body/div/div[2]/div/div[2]/div/div/div/div[2]/div[3]/button[2]")).getText();
		System.out.println("grey button text is: " + greyButton);
		
		 //Close the browser
        driver.close();
	

	}

}
