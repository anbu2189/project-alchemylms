package session6;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity6_2 {

	public static void main(String[] args) {
	WebDriver dr = new FirefoxDriver();
	dr.get("https://training-support.net/selenium/tables");
	//Get columns
    List<WebElement> cols = dr.findElements(By.xpath("//table[@id='sortableTable']/thead/tr/th"));
    //Get rows
    List<WebElement> rows = dr.findElements(By.xpath("//table[@id='sortableTable']/tbody/tr"));

    //Number of columns
    System.out.println("Number of columns are: " + cols.size());
    //Number of rows
    System.out.println("Number of rows are: " + rows.size());

    //Cell value of second row, second column
    WebElement cellValueBefore = dr.findElement(By.xpath("//table[@id='sortableTable']/tbody/tr[2]/td[2]"));
    System.out.println("Second row, second column value(Before sorting): " + cellValueBefore.getText());

    //Sort the table
    dr.findElement(By.xpath("//table[@id='sortableTable']/thead/tr/th[2]")).click();

    //Print the value again
    WebElement cellValueAfter = dr.findElement(By.xpath("//table[@id='sortableTable']/tbody/tr[2]/td[2]"));
    System.out.println("Second row, second column value(After sorting): " + cellValueAfter.getText());

    //Print footer cell value
    WebElement footer = dr.findElement(By.xpath("//table[@id='sortableTable']/tfoot/tr"));
    System.out.println("Table footer values: " + footer.getText());

    //Close browser
    dr.close();

	}

}
