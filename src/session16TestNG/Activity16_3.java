package session16TestNG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Activity16_3 {
	 WebDriver driver; 
	
@BeforeClass
	  public void beforeClass() {
		driver = new FirefoxDriver(); 	 
	  }	
 
@Test
  public void Test1() {
	//Find the checkbox
      WebElement checkbox = driver.findElement(By.xpath("//input[@type='checkbox']"));
      System.out.println("The checkbox Input is displayed: " + checkbox.isDisplayed());
      Assert.assertTrue(checkbox. isDisplayed());
		driver.findElement(By.id("toggleCheckbox")).click();
      System.out.println("The checkbox Input is displayed: " + checkbox.isDisplayed());
      Assert.assertFalse(checkbox. isDisplayed());
      }

  @AfterClass
  public void afterClass() {
	  driver.close();
  }

}
