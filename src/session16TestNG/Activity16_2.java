package session16TestNG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Activity16_2 {
WebDriver driver;	
	
@BeforeClass
public void beforeClass() {
	driver = new FirefoxDriver();
	//Open browser
    driver.get("https://www.training-support.net/selenium/login-form");
}  

@Test
  public void ValidloginTest() {
	//Find the username and password fields and Enter credentials
	WebElement UN = driver.findElement(By.id("username"));
	WebElement PW = driver.findElement(By.id("password"));
	
	UN.sendKeys("admin");
	PW.sendKeys("password");
	 
    //Click login
	driver.findElement(By.xpath("//button[@type = 'submit']")).click();
	
    //Read login message
    String loginMessage = driver.findElement(By.id("action-confirmation")).getText();
	Assert.assertEquals("Welcome Back, admin",loginMessage);
	
  }
 
@Test
 public void InvalidloginTest() {
	//Find the username and password fields and Enter credentials
		
	WebElement UN1 = driver.findElement(By.id("username"));
	WebElement PW1 = driver.findElement(By.id("password"));
	
	UN1.clear();
	UN1.sendKeys("username");
	PW1.clear();
	PW1.sendKeys("password");
		 
	  //Click login
		driver.findElement(By.xpath("//button[@type = 'submit']")).click();
	
	//Read login message
	    String Invalidlogin = driver.findElement(By.cssSelector("div#action-confirmation.ui.massive.red.label")).getText();
		Assert.assertEquals(Invalidlogin, "Invalid Credentials");

}
  @AfterClass
  public void afterClass() {
	  //Close browser
      //driver.close();
  }

}
