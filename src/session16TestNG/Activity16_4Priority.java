package session16TestNG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
public class Activity16_4Priority {
    WebDriver driver;
    @BeforeClass
    public void beforeClass() {
        driver = new FirefoxDriver();
        //Open browser
        driver.get("https://www.training-support.net/selenium/dynamic-controls");
    }
		@Test (priority=0)
      public void Testdisplay(){
        //Find the checkbox
        WebElement checkbox = driver.findElement(By.xpath("//input[@type='checkbox']"));
        System.out.println("The checkbox Input is displayed: " + checkbox.isDisplayed());
        Assert.assertTrue(checkbox. isDisplayed());
		}
		@Test (priority=1)
        public void Testdissappear(){
            //Find the checkbox
            WebElement checkbox = driver.findElement(By.xpath("//input[@type='checkbox']"));
            System.out.println("The checkbox Input is displayed: " + checkbox.isDisplayed());
            Assert.assertTrue(checkbox. isDisplayed());
		driver.findElement(By.id("toggleCheckbox")).click();
        System.out.println("The checkbox Input is displayed: " + checkbox.isDisplayed());
        Assert.assertFalse(checkbox. isDisplayed());
        }
        @AfterClass
        public void afterClass() {
            //Close browser
            driver.close();
    }
}