package liveproject1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Verify the title of the first info box
		//Goal: Read the title of the first info box on the website and verify the text
		
		//a. Open a browser.
		WebDriver driver = new FirefoxDriver();
		
		//b. Navigate to �https://alchemy.hguy.co/jobs�. 
		driver.get("https://alchemy.hguy.co/lms");
		WebElement firstinfotitle = driver.findElement(By.cssSelector("h3.uagb-ifb-title"));
		String infotext = firstinfotitle.getText();
		System.out.println("The First Info box title is :" + infotext);
		
		//Using the contentEquals() method
		boolean contentEqualsVerify = infotext.contentEquals("Actionable Training");
		System.out.println("Title equals actual text : " + contentEqualsVerify);
		
		//e. If it matches, close the browser.
		driver.close();
		
		
	}

}
