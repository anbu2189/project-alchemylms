package liveproject1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Activity12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Use XPath to find an element on the page
		//Goal: Using XPath to find elements on the page and complete a lesson.
		
		//a. Open the browser to the All Courses page of Alchemy LMS site.
		WebDriver driver = new FirefoxDriver();
		WebDriverWait wait = new WebDriverWait (driver,10);
		driver.get("https://alchemy.hguy.co/lms/all-courses/");
		
		//b.Find a course to open using XPath notations.
		driver.findElement(By.xpath("//article[@id='post-71']/a/img")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText("Login to Enroll")));
		driver.findElement(By.partialLinkText("Login to Enroll")).click();
		driver.findElement(By.xpath("//input[@id='user_login']")).sendKeys("root");
		driver.findElement(By.xpath("//input[@id='user_pass']")).sendKeys("pa$$w0rd");
		driver.findElement(By.xpath("//input[@id='wp-submit']")).click();
		
		//c. Find a lesson in that course and open it using XPath notations.
		driver.findElement(By.xpath("//div[@class = 'ld-item-title']")).click();
		
		//d. Find the Mark Complete button on the page using XPath and click it.
		//driver.findElement(By.xpath("//input[@class = 'learndash_mark_complete_button']")).click();
		
		//e. Check progress of course by finding the progress bar on the page using XPath
		String verifyText = driver.findElement(By.cssSelector("div.ld-status.ld-status-complete")).getText();
		System.out.println("The status of 'Deliverability of your Emails' topic is :" + verifyText);
		
		//f. Close the browser
		driver.close();

	}

}
