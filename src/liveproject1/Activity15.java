package liveproject1;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;


public class Activity15 {
	
  WebDriver driver;
  WebDriverWait wait;
    
  @BeforeClass
  public void beforeClass() {
  driver = new FirefoxDriver();
  wait = new WebDriverWait (driver,10);
  //Open browser
  driver.get("https://alchemy.hguy.co/lms/all-courses/");
  }
  
  @Test
  public void f() {
  //b. Select any course and open it.
  driver.findElement(By.xpath("//article[@id='post-24042']/a/img")).click();
  driver.findElement(By.linkText("My Account")).click();
  }
  
  @Test
  public void login() {
  //Click login button
  wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[@id='uagb-column-e9d225cb-cee9-4e02-a12d-073d5f051e91']/div[2]/div[2]/a")));
  driver.findElement(By.xpath("//div[@id='uagb-column-e9d225cb-cee9-4e02-a12d-073d5f051e91']/div[2]/div[2]/a")).click();
  driver.findElement(By.xpath("//input[@id='user_login']")).sendKeys("root");
  //driver.findElement(By.xpath("//a[@class='ld-login ld-login ld-login-text ld-login-button ld-button']")).sendKeys("root");
  driver.findElement(By.xpath("//input[@id='user_pass']")).sendKeys("pa$$w0rd");
  driver.findElement(By.xpath("//input[@id='wp-submit']")).click();
  }
  
  @Test
  public void opentopic() {
  wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[@id='ld-course-list-item-24042']/div/a/span")));
  driver.findElement(By.xpath("//div[@id='ld-course-list-item-24042']/div/a/span")).click();  
  wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id='ld-expand-283']/div/a/div[2]")));
  driver.findElement(By.xpath("//*[@id='ld-expand-283']/div/a/div[2]")).click();
  String verifyText = driver.findElement(By.cssSelector("div.ld-status.ld-status-complete.ld-secondary-background")).getText();
  System.out.println("The status of 'Effective writing & Promoting your content' topic is :" + verifyText);	  
  Reporter.log("Effective writing status: " + verifyText);   
  Assert.assertEquals(verifyText, "COMPLETE");
  }
  
  @Test
  public void verifysubtopic() {
  wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id='ld-lesson-list-24042']/div[2]/div/a/div[2]")));
  driver.findElement(By.xpath("//*[@id='ld-lesson-list-24042']/div[2]/div/a/div[2]")).click();
  String verifyText1 = driver.findElement(By.cssSelector("div.ld-status.ld-status-complete.ld-secondary-background")).getText();
  System.out.println("The status of 'Analyze content & Develop writing Strategies' topic is :" + verifyText1);	  
  Reporter.log("Analyze content status: " + verifyText1);   
  Assert.assertEquals(verifyText1, "COMPLETE");	 
  driver.navigate().back();
  driver.navigate().back();
  driver.navigate().back();
  wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("div.ld-status.ld-status-complete.ld-secondary-background")));
  String verifyText2 = driver.findElement(By.cssSelector("div.ld-status.ld-status-complete.ld-secondary-background")).getText();
  System.out.println("The status of 'Content Marketing' topic is :" + verifyText2);	  
  Reporter.log("Content Marketing status: " + verifyText2);   
  Assert.assertEquals(verifyText2, "COMPLETE");
  driver.findElement(By.cssSelector("a.ld-logout")).click();
  }
  
  @AfterClass
  public void afterClass() {
  //g.Close the browser
  driver.close();
  }

}
