package liveproject1;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Activity14 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		//Using only keyboard actions
		//Goal: Search for a course and complete it using only keyboard actions. No Clicks.
		
		//a. Open the browser to the My Account page of Alchemy LMS site.
		WebDriver driver = new FirefoxDriver();
		WebDriverWait wait = new WebDriverWait (driver,10);
		driver.get("https://alchemy.hguy.co/lms/my-account/");
		Actions action = new Actions(driver);
		
		//b. Open a course using only keyboard actions.
		WebElement login = driver.findElement(By.xpath("//*[@class='ld-login ld-login ld-login-text ld-login-button ld-button']"));
		action.sendKeys(login, Keys.ENTER).build().perform();
		WebElement username = driver.findElement(By.xpath("//input[@id='user_login']"));
		action.sendKeys(username,"root").build().perform();
		WebElement password = driver.findElement(By.xpath("//input[@id='user_pass']"));
		action.sendKeys(password,"pa$$w0rd").build().perform();
		WebElement submit = driver.findElement(By.xpath("//input[@id='wp-submit']"));
		action.sendKeys(submit, Keys.ENTER).build().perform();
		WebElement allcourses = driver.findElement(By.xpath("//*[@id='menu-item-1508']/a"));
		action.sendKeys(allcourses, Keys.ENTER).build().perform();
		
		//c.Find the lessons in the course and hit ENTER on them
		//wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#ld-course-list-item-24042 > div:nth-child(1) > a:nth-child(1) > span:nth-child(2)")));
		//action.sendKeys(Keys.PAGE_DOWN).build().perform();
		//action.moveByOffset(631, 379);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		  js.executeScript("window.scrollBy(221,714)");
		WebElement course1 = driver.findElement(By.xpath("//*[@id='post-69']/a/img"));
		action.sendKeys(course1, Keys.ENTER).build().perform();
		JavascriptExecutor js1 = (JavascriptExecutor) driver;
		js1.executeScript("window.scrollBy(353,-425)");
		WebElement login1 = driver.findElement(By.xpath("//*[@href='#login']"));
		action.sendKeys(login1, Keys.ENTER).build().perform();
		WebElement username1 = driver.findElement(By.xpath("//input[@id='user_login']"));
		action.sendKeys(username1,"root").build().perform();
		WebElement password1 = driver.findElement(By.xpath("//input[@id='user_pass']"));
		action.sendKeys(password1,"pa$$w0rd").build().perform();
		WebElement submit1 = driver.findElement(By.xpath("//input[@id='wp-submit']"));
		action.sendKeys(submit1, Keys.ENTER).build().perform();
		
		
		
		//d.Find the Mark Complete button and hit ENTER on it.
		
		//e.Repeat for each lesson in the course ;Check progress of course and print it.

		//f. Close the browser.
		//driver.close();
		
	}

}
