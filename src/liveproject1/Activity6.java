package liveproject1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Logging into the site
		//Goal: Open the website and log-in using the provided credentials.

		//a. Open a browser.
		WebDriver driver = new FirefoxDriver();
		//WebDriverWait wait = new WebDriverWait(driver, 10);
		
		//b. Navigate to �https://alchemy.hguy.co/jobs�. 
		driver.get("https://alchemy.hguy.co/lms");
		
		//c.Find the navigation bar.
		WebElement account = driver.findElement(By.partialLinkText("My Account"));
		account.click();
		
		//d. Read the page title 
		String PageTitle = driver.getTitle();
		System.out.println("The page title is :" + PageTitle);
		
		//e. verify that you are on the correct page.
		boolean equalsVerify = PageTitle.equals("My Account � Alchemy LMS");
		System.out.println("Title equals actual text : " + equalsVerify);
		
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(".ld-login")));
		//f. Find the �Login� button on the page and click it.
		driver.findElement(By.xpath("//*[@id=\"uagb-column-e9d225cb-cee9-4e02-a12d-073d5f051e91\"]/div[2]/div[2]/a")).click();
		
		//g. Find the username field of the login form and enter the username into that field.
		driver.findElement(By.xpath("//input[@id='user_login']")).sendKeys("root");
		
		//h. Find the password field of the login form and enter the username into that field.
		driver.findElement(By.xpath("//input[@id='user_pass']")).sendKeys("pa$$w0rd");
		
		//i. Find the login button and click it.
		driver.findElement(By.id("wp-submit")).click();
		
		//j. Verify that you have logged in
		String loginverify = driver.findElement(By.partialLinkText("Howdy,")).getText();
		System.out.println("The after login text is :" + loginverify);
		
		//k. Close the browser
		driver.close();
	
	}

}
