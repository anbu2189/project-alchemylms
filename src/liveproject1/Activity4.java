package liveproject1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Verify the title of the second most popular course
		//Goal: Read the title of the second most popular course on the website and verify the text
		
		//a. Open a browser.
		WebDriver driver = new FirefoxDriver();
		
		//b. Navigate to �https://alchemy.hguy.co/jobs�. 
		driver.get("https://alchemy.hguy.co/lms");
		
		//c. Get the title of the second most popular course.
		String Title = driver.findElement(By.xpath("//*[@id=\"post-71\"]/div[2]/h3")).getText();
		System.out.println("The title of second most popular course is: " + Title);
		
		//d.Make sure it matches �Email Marketing Strategies� exactly.
		//Using the Equals() method
		boolean equalsVerify = Title.equals("Email Marketing Strategies");
		System.out.println("Title equals actual text : " + equalsVerify);
		
		//e. If it matches, close the browser.
		driver.close();
	}

}
