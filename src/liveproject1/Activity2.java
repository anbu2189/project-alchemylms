package liveproject1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Verify the website heading
		//Goal: Read the heading of the website and verify the text
		
		//a. Open a browser.
		WebDriver driver = new FirefoxDriver();
		
		//b. Navigate to �https://alchemy.hguy.co/jobs�. 
		driver.get("https://alchemy.hguy.co/lms");
		
		//c. Get the heading of the webpage.
		String heading = driver.findElement(By.cssSelector(" h1.uagb-ifb-title")).getText();
		System.out.println("The Heading is :" + heading);
		
		//d. Make sure it matches �Learn from Industry Experts� exactly.
		//Assert.assertEquals(heading, "Learn from Industry Experts");
		//Assert.assertTrue(true,"Learn from Industry Experts");
		
		//Using the contentEquals() method
		boolean contentEqualsVerify = heading.contentEquals("Learn from Industry Experts");
		System.out.println("Title equals Heading : " + contentEqualsVerify);
		
		//e. If it matches, close the browser.
		driver.close();
		
	}

}
