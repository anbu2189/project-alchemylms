package liveproject1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity11 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Complete an entire course
		//Goal: Navigate to a particular course and complete all lessons and topics in it.
		
		//a. Open the browser to the All Courses page of Alchemy LMS site.
		WebDriver driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/lms/all-courses/");
		
		//b. Select any course and open it.
		driver.findElement(By.xpath("//article[@id='post-24042']/a/img")).click();
		
		driver.findElement(By.linkText("My Account")).click();
		driver.findElement(By.xpath("//div[@id='uagb-column-e9d225cb-cee9-4e02-a12d-073d5f051e91']/div[2]/div[2]/a")).click();
		
		//c. Click on a lesson in the course. Verify the title of the course.
		
		driver.findElement(By.xpath("//input[@id='user_login']")).sendKeys("root");
		driver.findElement(By.xpath("//input[@id='user_pass']")).sendKeys("pa$$w0rd");
		driver.findElement(By.xpath("//input[@id='wp-submit']")).click();
		driver.findElement(By.xpath("//article[@id='post-24042']/a/img")).click();
		driver.findElement(By.cssSelector("div.ld-item-list-item:nth-child(2) > div:nth-child(1) > a:nth-child(1) > div:nth-child(2)")).click();
		
		//d. Open a topic in that lesson. Mark it as complete.
		driver.findElement(By.cssSelector("div.ld-table-list-items:nth-child(1) > div:nth-child(1) > a:nth-child(1) > div:nth-child(2)")).click();
		String verifyText = driver.findElement(By.cssSelector("div.ld-status.ld-status-complete")).getText();
		System.out.println("The status of 'Growth Hacking with your content' topic is :" + verifyText);
		
		//e. Perform the above steps for all lessons and topics in the course.
		//div.ld-table-list-items:nth-child(1) > div:nth-child(2) > a:nth-child(1) > div:nth-child(2)
		//div.ld-table-list-item:nth-child(2) > a:nth-child(1) > div:nth-child(2)
		driver.findElement(By.cssSelector("div.ld-table-list-items:nth-child(1) > div:nth-child(2) > a:nth-child(1) > div:nth-child(2)")).click();
		String verifyText1 = driver.findElement(By.cssSelector("div.ld-status.ld-status-complete")).getText();
		System.out.println("The status of 'The Power of Effective content' topic is :" + verifyText1);
		
		//g.Close the browser
		driver.close();
	
	}

}






