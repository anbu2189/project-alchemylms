package liveproject1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Navigate to another page
		//Goal: Navigate to the �My Account� page on the site.

		//a. Open a browser.
		WebDriver driver = new FirefoxDriver();
		
		//b. Navigate to �https://alchemy.hguy.co/jobs�. 
		driver.get("https://alchemy.hguy.co/lms");
		
		//c.Find the navigation bar.
		//driver.findElement(By.partialLinkText("My Account")).click();
		WebElement account = driver.findElement(By.partialLinkText("My Account"));
		account.click();
		
		//d. Read the page title 
		String PageTitle = driver.getTitle();
		System.out.println("The page title is :" + PageTitle);
		
		//e. verify that you are on the correct page.
		boolean equalsVerify = PageTitle.equals("My Account � Alchemy LMS");
		System.out.println("Title equals actual text : " + equalsVerify);
		
		//f. Close the browser.
		driver.close();
	}

}
