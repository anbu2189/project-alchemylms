package liveproject1;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		//a. Open a browser.
		WebDriver driver = new FirefoxDriver();
				
		//b. Navigate to �https://alchemy.hguy.co/jobs�. 
		driver.get("https://alchemy.hguy.co/lms");
				
		//c.Find the navigation bar.
		//driver.findElement(By.partialLinkText("My Account")).click();
		WebElement AllCourses = driver.findElement(By.partialLinkText("All Courses"));
		AllCourses.click();
		
		//d.Find all the courses on the page.
		String firstcourse = driver.findElement(By.xpath("//h3[@class='entry-title']")).getText();
		String secondcourse = driver.findElement(By.xpath("//*[@id=\"post-71\"]/div[2]/h3")).getText();
		String thirdcourse = driver.findElement(By.xpath("//*[@id='post-24042']/div[2]/h3")).getText();
		
		//e. Print the number of courses on the page.
		System.out.println("The first course on page is :" + firstcourse);
		System.out.println("The second course on page is :" + secondcourse);
		System.out.println("The third course on page is :" + thirdcourse);
		
		List<WebElement> courses = driver.findElements(By.className("entry-title"));	
		System.out.println("number of courses on the page is:"+courses.size() );
	
		driver.close();
		
	}

}
