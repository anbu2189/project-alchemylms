package liveproject1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Contact the admin
		//Goal: Navigate to the �Contact Us� page and complete the form.
		
		//a. Open a browser.
		WebDriver driver = new FirefoxDriver();
				
		//b. Navigate to �https://alchemy.hguy.co/jobs�. 
		driver.get("https://alchemy.hguy.co/lms");
				
		//c.Find the navigation bar.
		//driver.findElement(By.partialLinkText("My Account")).click();
		driver.findElement(By.xpath("//div[@class = 'ast-main-header-bar-alignment']/div"));
		
		//d. Select the menu item that says �Contact� and click it.
		driver.findElement(By.xpath("//li[@id= 'menu-item-1506']/a")).click();
		
		//e. Find the contact form fields (Full Name, email, etc.)
		
		WebElement FullName = driver.findElement(By.name("wpforms[fields][0]"));
		WebElement Email = driver.findElement(By.xpath("//input[@id='wpforms-8-field_1']"));
		WebElement Subject = driver.findElement(By.xpath("//div[@id='wpforms-8-field_3-container']/input"));
		WebElement Message = driver.findElement(By.name("wpforms[fields][2]"));
		
		//f. Fill in the information and leave a message.
		FullName.sendKeys("Anbarasan");
		Email.sendKeys("abc@gmail.com");
		Subject.sendKeys("Test");
		Message.sendKeys("Test Message");
		
		//g. Click submit.
		driver.findElement(By.id("wpforms-submit-8")).click();
		
		//h. Read and print the message displayed after submission.
		String Displaymsg = driver.findElement(By.xpath("//div[@id='wpforms-confirmation-8']/p")).getText();
		System.out.println("The displayed message is :" + Displaymsg);
		//driver.findElement(By.partialLinkText("contacting us!"))
		
		//i. Close the browser
		driver.close();
	
	}

}
