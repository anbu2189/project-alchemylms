package liveproject1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Activity10 {

	public static void main(String[] args) {
	//Complete a topic in a lesson
	//Goal: Navigate to a particular lesson and complete a topic in it
	//a. Open a browser.
	WebDriver driver = new FirefoxDriver();
	WebDriverWait wait = new WebDriverWait(driver,10);
	
	//b. Navigate to �https://alchemy.hguy.co/jobs�. 
	driver.get("https://alchemy.hguy.co/lms");
								
	//c.Find the navigation bar.
	driver.findElement(By.xpath("//div[@class = 'ast-main-header-bar-alignment']/div"));
				
	//d. Select the menu item that says �All Courses� and click it.
	WebElement AllCourses = driver.findElement(By.partialLinkText("All Courses"));
	AllCourses.click();
				
	//e. Select any course and open it.
	driver.findElement(By.xpath("//article[@id='post-71']/a/img")).click();
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText("Login to Enroll")));
	driver.findElement(By.partialLinkText("Login to Enroll")).click();
	driver.findElement(By.xpath("//input[@id='user_login']")).sendKeys("root");
	driver.findElement(By.xpath("//input[@id='user_pass']")).sendKeys("pa$$w0rd");
	driver.findElement(By.xpath("//input[@id='wp-submit']")).click();
	
	//f. Click on a lesson in the course. Verify the title of the course.
	driver.findElement(By.className("ld-item-title")).click();
	
	String PageTitle = driver.getTitle();
	System.out.println("The Page Title is :" + PageTitle);			
		
	//g. Open a topic in that lesson. Mark it as complete.
	driver.findElement(By.xpath("//div[@class = 'ld-lesson-title']")).click();
	
	//h.Mark all the topics in the lesson as complete (if available).
	String verifyText = driver.findElement(By.cssSelector("div.ld-status.ld-status-complete")).getText();
	System.out.println("The text displayed is :" + verifyText);
	
	//i. close the browser
	driver.close();
	
	}	
}
