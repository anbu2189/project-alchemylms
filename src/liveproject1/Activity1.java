package liveproject1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;

public class Activity1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Verify the website title
		//Goal: Read the title of the website and verify the text
		
		// Create a new instance of the Firefox driver
		//a. Open a browser.
		WebDriver driver = new FirefoxDriver();
		
		//b. Navigate to �https://alchemy.hguy.co/jobs�. 
		driver.get("https://alchemy.hguy.co/lms");
		
		//c. Get the title of the website.
		String PageTitle = driver.getTitle();
		System.out.println("The Page Title is :" + PageTitle);
		
		//d. Make sure it matches �Alchemy Jobs � Job Board Application� exactly.
		Assert.assertEquals(PageTitle, "Alchemy LMS � An LMS Application");
		
		//e. If it matches, close the browser.
		driver.close();
	}

}
