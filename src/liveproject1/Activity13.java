package liveproject1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Activity13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Complete an entire course using only XPath
		//Goal: Finding elements on the page and using nothing but XPath notations to complete a course
		
		//a. Open the browser to the All Courses page of Alchemy LMS site.
		WebDriver driver = new FirefoxDriver();
		WebDriverWait wait = new WebDriverWait (driver,10);
		driver.get("https://alchemy.hguy.co/lms/all-courses/");
				
		//b.Find a course to open using XPath notations.
		driver.findElement(By.xpath("//article[@id='post-71']/a/img")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText("Login to Enroll")));
		driver.findElement(By.partialLinkText("Login to Enroll")).click();
		driver.findElement(By.xpath("//input[@id='user_login']")).sendKeys("root");
		driver.findElement(By.xpath("//input[@id='user_pass']")).sendKeys("pa$$w0rd");
		driver.findElement(By.xpath("//input[@id='wp-submit']")).click();
		
		//c. Find a lesson in that course and open it using XPath notations.
		driver.findElement(By.xpath("//div[@class = 'ld-item-title']")).click();
		String verifyText = driver.findElement(By.cssSelector("div.ld-status.ld-status-complete")).getText();
		System.out.println("The status of 'Deliverability of your Emails' topic is :" + verifyText);
		
		driver.findElement(By.xpath("//*[@id='ld-lesson-list-71']/div[2]/div/a/div[2]")).click();
		//driver.findElement(By.xpath("//*[@id='learndash_post_24186']/div/div[3]/div[2]/form/input[4]")).click();
		
		String verifyText1 = driver.findElement(By.cssSelector("div.ld-status.ld-status-complete")).getText();
		System.out.println("The status of 'Improving & Designing Market Emails is :" + verifyText1);
	
		//e. Mark the lesson as complete and verify progress of the course.
		String verifyText2 = driver.findElement(By.xpath("//*[@class='ld-status ld-status-complete ld-secondary-background']")).getText();
		System.out.println("The status of 'Email marketing strategies is :" + verifyText2);
		
		//f. Close the browser.
		driver.close();
	
	}

}
