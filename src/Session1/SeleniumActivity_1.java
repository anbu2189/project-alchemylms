package Session1;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SeleniumActivity_1 {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = new FirefoxDriver();
		driver.get("https://www.google.com");
		
		//Or 
		//String URL = "http://www.google.com";
		//driver.get(URL);
		
		
		Thread.sleep(1000);
		driver.close();
		    }
		}
		
