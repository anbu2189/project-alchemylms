package session3;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class Activity3 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		//Create a new instance of the firefox Driver
		WebDriver driver = new FirefoxDriver();
		
		//Open the browser
		driver.get("https://www.training-support.net/selenium/simple-form");
		
		// Check the title & print the page
    	String Pagetitle = driver.getTitle();
      	System.out.println("Page title is: " + Pagetitle);
    	
		//Find the Input fields
      	WebElement firstName = driver.findElement(By.id("firstName"));
      	WebElement lastName = driver.findElement(By.id("lastName"));

		//Enter text in input fields
      	firstName.sendKeys("Anbarasan");
      	lastName.sendKeys("Alagarsamy");
	
      	//Enter e-mail
      	WebElement email = driver.findElement(By.id("email"));
      	email.sendKeys("anbuadonis2010@gmail.com");
      	//Enter contact number
      	WebElement number = driver.findElement(By.id("number"));
      	number.sendKeys("0123456789");
	
      	//Submit the form    
      	//CSS - driver.findElement(By.cssSelector(".ui.green.button")).click();
      	
      	
      	driver.findElement(By.xpath("//*[@id=\"simpleForm\"]")).click();
      	Thread.sleep(1000);
      	
      	// xpath - ("//input[@type='submit']")
      	
      	//close the browser
      	driver.close();
      	
	}

}
