package session7;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class UnorderedListDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriver dr = new FirefoxDriver();
		dr.get("https://training-support.net/selenium/lists");
		//Find Unordered list
		WebElement unordered = dr.findElement(By.xpath("//ul"));
		System.out.println("Unordered list: \n" + unordered.getText());
		System.out.println("=======================================================");
		//Find Unordered 2nd list item
		WebElement unorderedlistitem = dr.findElement(By.xpath("//ul/li[2]"));
		System.out.println("Unordered 2nd list item: " + unorderedlistitem.getText());
		System.out.println("=======================================================");
		//Find nested unordered list
		WebElement nestedunordered = dr.findElement(By.xpath("//ul/li[3]/ul"));
		System.out.println("Nested Unordered list: \n" + nestedunordered.getText());
		System.out.println("=======================================================");
		//Find nested unordered list item
		WebElement nestedunordered2nd = dr.findElement(By.xpath("//ul/li[3]/ul/li[2]"));
		System.out.println("Nested Unordered list 2nd item: " + nestedunordered2nd.getText());
		System.out.println("=======================================================");
		
		dr.close();
		
	}
}
		


