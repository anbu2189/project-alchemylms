package session5;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity5_3 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		// Create a new instance of the Firefox driver
		WebDriver driver = new FirefoxDriver();
		
		//Open the browser
		driver.get("https://training-support.net/selenium/dynamic-controls");

		//Find the page title and Print it
		String PageTitle = driver.getTitle();
		System.out.println("The Page Title is: " + PageTitle);
		
		//Find the text field
		WebElement textInput = driver.findElement(By.xpath("//input[@id='input-text']"));
		System.out.println("The checkbox is selected: " + textInput.isEnabled());
		
		driver.findElement(By.xpath("//*[@id='toggleInput']")).click();
		
		System.out.println("The checkbox is selected: " + textInput.isEnabled());
		Thread.sleep(2000);
		
		//Close the browser
		driver.close();
	}

}
