package session5;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity5_2 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		// Create a new instance of the Firefox driver
		WebDriver driver = new FirefoxDriver();
		
		//Open the browser
		driver.get("https://training-support.net/selenium/dynamic-controls");
		
		//Find the page title and Print it
		String PageTitle = driver.getTitle();
		System.out.println("The Page title is: " + PageTitle );
		
		//Find the input checkbox element
		WebElement checkboxInput =driver.findElement(By.xpath("//input[@type='checkbox']"));
		System.out.println("The checkbox input is selected: " + checkboxInput.isSelected());
		
		Thread.sleep(2000);
		
		checkboxInput.click();
		
		Thread.sleep(2000);
		
		System.out.println("The checkbox input is selected: " + checkboxInput.isSelected());
		
		Thread.sleep(2000);
		
		
		//Close the browser
		driver.close();
	}

}
