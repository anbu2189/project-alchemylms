package session5;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
public class Activity5_4 {
public static void main(String[] args) throws InterruptedException {
		
		WebDriver dr = new FirefoxDriver();
		dr.get("https://www.training-support.net/selenium/login-form");
		dr.findElement(By.id("username")).sendKeys("admin");
		Thread.sleep(2000);
		dr.findElement(By.id("password")).sendKeys("password");
		Thread.sleep(2000);
		dr.findElement(By.xpath("//button[@type='submit']")).click();
		WebElement msg= dr.findElement(By.id("action-confirmation"));
		System.out.println(msg.getText());
		Thread.sleep(2000);
		
		System.out.println("Is Confirmation message displayed?: " + msg.isDisplayed());
		System.out.println("Is Confirmation message correct?: " + msg.getText().equals("Welcome Back, admin"));
		
		dr.close();

	}

}
