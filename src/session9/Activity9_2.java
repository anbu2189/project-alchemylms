package session9;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity9_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		WebDriver dr = new FirefoxDriver();
		dr.get("https://training-support.net/selenium/dynamic-attributes");
		String title = dr.getTitle();
		System.out.println("Title of the page is: " + title);
		
		WebElement username = dr.findElement(By.xpath("//input[contains(@class,'-username')]"));
		WebElement password = dr.findElement(By.xpath("//input[contains(@class,'-password')]"));
		WebElement confirmpassword = dr.findElement(By.xpath("//label[text() = 'Confirm Password']/following::input"));
		WebElement email = dr.findElement(By.xpath("//label[contains(text(),'mail')]/following-sibling::input"));
		
		username.sendKeys("Anbu");
		password.sendKeys("Password");
		confirmpassword.sendKeys("Password");
		email.sendKeys("anbu_email@abc.com");
		
		dr.findElement(By.xpath("//button[contains(text(),'Sign Up')]")).click();
		
		String msg = dr.findElement(By.xpath("//div[@id='action-confirmation']")).getText();
		System.out.println("The Sign up success message is: " + msg);
		
		dr.close();
		
	}

}
