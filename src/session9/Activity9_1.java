package session9;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity9_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriver dr = new FirefoxDriver();
		dr.get("https://training-support.net/selenium/dynamic-attributes");
		String title = dr.getTitle();
		System.out.println("Title of the page is: " + title);

		WebElement username = dr.findElement(By.xpath("//input[starts-with(@class, 'username-')]"));
		username.sendKeys("admin");
		WebElement password = dr.findElement(By.xpath("//input[starts-with(@class, 'password-')]"));
		password.sendKeys("password");
		dr.findElement(By.xpath("//button[contains(text(), 'Log in')]")).click();
		
		String msg = dr.findElement(By.xpath("//div[contains(text(),'Welcome Back, admin')]")).getText();
		System.out.println("Login Message is: " + msg);
		
		dr.close();
		
	}

}
