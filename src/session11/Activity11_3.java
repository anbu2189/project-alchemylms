package session11;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity11_3 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		WebDriver dr = new FirefoxDriver();
		dr.get("https://www.training-support.net/selenium/javascript-alerts");
		String Title = dr.getTitle();
		System.out.println("The Title of the page is: " + Title);
		//System.out.println("Page title is: " + driver.getTitle());
		
		dr.findElement(By.cssSelector("button#prompt")).click();
		
		Alert Prompt = dr.switchTo().alert();
		String prompttext = Prompt.getText();
		System.out.println("Prompt text is: " + prompttext);
		
		//System.out.println("Prompt text is: " + Prompt.getText());
		
		Thread.sleep(2000);
		
		Prompt.sendKeys("Yes, you are!");
		
		Thread.sleep(2000);
		Prompt.accept();
		
		dr.close();

	}

}
