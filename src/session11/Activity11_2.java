package session11;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity11_2 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		WebDriver dr = new FirefoxDriver();
		dr.get("https://www.training-support.net/selenium/javascript-alerts");
		String Title = dr.getTitle();
		System.out.println("The Title of the page is:" + Title);
		
		dr.findElement(By.cssSelector("button#confirm")).click();
		//Switch to alert window
		Alert confirmalert = dr.switchTo().alert();
		//Get text in the alert box and print it
		String ConfirmAlerttext = confirmalert.getText();
		System.out.println("Confirm Alert Text is: " + ConfirmAlerttext);
		
		Thread.sleep(2000);
		
		//Close the alert box
		confirmalert.accept();
		
		dr.findElement(By.cssSelector("button#confirm")).click();
		
		Thread.sleep(2000);
		//Close the alert box
		confirmalert.dismiss();
		
		dr.close();
		
	}

}
