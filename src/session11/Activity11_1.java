package session11;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity11_1 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		WebDriver dr = new FirefoxDriver();
		dr.get("https://www.training-support.net/selenium/javascript-alerts");
		String Title = dr.getTitle();
		System.out.println("Title of the page is: " + Title);
		dr.findElement(By.id("simple")).click();
		//dr.findElement(By.cssSelector("button#simple"));
		
		//Switch to alert window
		Alert SimpleAlert = dr.switchTo().alert();
		
		//Get text in the alert box and print it
		String alerttext = SimpleAlert.getText();
		System.out.println("Alert text is: " + 	alerttext);
		
		Thread.sleep(2000);
		
		//Close the alert box
		SimpleAlert.accept();
		dr.close();
	
		
	}

}
