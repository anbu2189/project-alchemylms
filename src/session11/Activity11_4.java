package session11;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Activity11_4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		WebDriver dr = new FirefoxDriver();
		WebDriverWait wait = new WebDriverWait(dr, 5);
		
		dr.get("https://www.training-support.net/selenium/tab-opener");
		System.out.println("The title of the page is: " + dr.getTitle());
		
		//Get parent window handle
		System.out.println("Parent Window is: " + dr.getWindowHandle());
		
		//Find link to open new tab and click it
		dr.findElement(By.cssSelector("#launcher")).click();
		wait.until(ExpectedConditions.numberOfWindowsToBe(2));
		
		//Get Window handles
		System.out.println("All Window handles are: " + dr.getWindowHandles());
		
		 //Loop through each handle
        for (String handle : dr.getWindowHandles()) {
            dr.switchTo().window(handle);
        }
 
      //Print the handle of the current window
        System.out.println("Current window handle: " + dr.getWindowHandle());
 
        //Wait for page to load completely
        wait.until(ExpectedConditions.titleIs("Newtab"));
 
        //Print New Tab Title
        System.out.println("New Tab Title is: " + dr.getTitle());
        
      //Wait for the elements to load completely
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.content")));
        
      //Get heading on new page
        String newTabText = dr.findElement(By.xpath("//div[@class ='content']")).getText();
        System.out.println("New tab heading is: " + newTabText);
 
        //Open Another Tab
        dr.findElement(By.linkText("Open Another One!")).click();
        wait.until(ExpectedConditions.numberOfWindowsToBe(3));
 
        //Make sure the new tab's handle is part of the handles set
        System.out.println("All window handles are: " + dr.getWindowHandles());
 
        //Loop through the handles set till we get to the newest handle
        for (String handle : dr.getWindowHandles()) {
            dr.switchTo().window(handle);
        }
 
        //Print the handle of the current window
        System.out.println("New tab handle: " + dr.getWindowHandle());
 
        //Wait for the newest tab to load completely
        wait.until(ExpectedConditions.titleIs("Newtab2"));
 
        //Print New Tab Title
        System.out.println("New Tab Title is: " + dr.getTitle());
 
        //Get heading on new page
        String newTabText1 = dr.findElement(By.cssSelector("div.content")).getText();
        System.out.println("New tab heading is: " + newTabText1);
 
        //Close the browser
        dr.quit();
	}

}
