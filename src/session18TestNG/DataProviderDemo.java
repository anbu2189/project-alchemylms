package session18TestNG;

import org.testng.annotations.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;

public class DataProviderDemo {
	WebDriver driver;
	WebDriverWait wait;

	  @BeforeClass
	  public void beforeClass() {
		  driver = new FirefoxDriver();
		  wait = new WebDriverWait(driver, 10);
		  //Open browser
		  driver.get("https://www.training-support.net/selenium/login-form"); 
	  }
	@DataProvider(name = "Authentication")
	public static Object[][] credentials() {  
		return new Object[][] { {"admin", "password" }, {"ADMIN", "PA$$word"} };
	}
  @Test (dataProvider = "Authentication")
  public void loginSuccessTest(String username, String password) {
	//Find username and password fields
      WebElement usernameField = driver.findElement(By.id("username"));
      WebElement passwordField = driver.findElement(By.id("password"));
      
      //Clear the input fields
      usernameField.clear();
      passwordField.clear();
      
      //Enter values
      usernameField.sendKeys(username);
      passwordField.sendKeys(password);
      
      //Click Log in
      driver.findElement(By.cssSelector("button[type='submit']")).click();
      
      //Get Message
      String loginMessage = driver.findElement(By.id("action-confirmation")).getText();
      
      //Assert Message
      if(username == "admin") {
    	  Assert.assertEquals(loginMessage, "Welcome Back, admin");  
      } else {
      Assert.assertEquals(loginMessage, "Invalid Credentials");}
  }
	  
  @AfterClass
  public void afterClass() {
	  //Close the browser
	  driver.close();
  }

}
