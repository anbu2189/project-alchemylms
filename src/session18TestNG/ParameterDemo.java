package session18TestNG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class ParameterDemo {
  WebDriver driver;
  WebDriverWait wait;
	
  @BeforeClass
  public void beforeClass() {
	  driver = new FirefoxDriver();
	  wait = new WebDriverWait(driver, 10);
	  
	  //Open browser
	  driver.get("https://www.training-support.net/selenium/login-form");
  }

  @Test (priority = 0)
  @Parameters({"correctUsername","correctPassword"})
  public void loginSuccessTestCase(String username, String password) {
      //Find username and password fields
      WebElement usernameField = driver.findElement(By.id("username"));
      WebElement passwordField = driver.findElement(By.id("password"));
      
      //Clear the input fields
      usernameField.clear();
      passwordField.clear();
      
      //Enter values
      usernameField.sendKeys(username);
      passwordField.sendKeys(password);
      
      //Click Log in
      driver.findElement(By.cssSelector("button[type='submit']")).click();
      
      //Assert Message
      String loginMessage = driver.findElement(By.id("action-confirmation")).getText();
      Assert.assertEquals(loginMessage, "Welcome Back, admin");
  }
  
  @Test (priority = 1)
  @Parameters({"wrongUsername","wrongPassword"})
  public void loginFailureTestCase(String username, String password) {
	  //Find username and password fields
	  WebElement usernameField = driver.findElement(By.id("username"));
      WebElement passwordField = driver.findElement(By.id("password"));
      
      //Clear the input fields
      usernameField.clear();
      passwordField.clear();
      
      //Enter values
      usernameField.sendKeys(username);
      passwordField.sendKeys(password);
	  
    //Click Log in
      driver.findElement(By.cssSelector("button[type='submit']")).click();
      
      //Assert Message
      String loginMessage = driver.findElement(By.id("action-confirmation")).getText();
      Assert.assertEquals(loginMessage, "Invalid Credentials");	  
  }
  
  @AfterClass
  public void afterClass() {
  
	  //close the browser
	  //driver.close();
  }

}
