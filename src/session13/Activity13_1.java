package session13;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity13_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		WebDriver dr = new FirefoxDriver();
		//Creating the JavascriptExecutor interface object by Type casting
		JavascriptExecutor js = (JavascriptExecutor) dr;
		//Open the browser
		dr.get("https://www.training-support.net");
		
		//Fetching the Domain Name of the site. Tostring() to change object to string.
		Object DomainName = js.executeScript("return document.domain;").toString();
		System.out.println("Domain name of the site is: " + DomainName);
		
		//Fetching the URL of the site. Tostring() to change object to string.
		Object URL = js.executeScript("return document.URL;").toString();
		System.out.println("The URL of the site is: " + URL);

		//fetch the Title name of the site. Tostring() to change object to string.
		Object PageTitle = js.executeScript("return document.title;").toString();
		System.out.println("The title of the page is: " + PageTitle);
		
		dr.close();
	}

}
