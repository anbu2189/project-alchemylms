package session13;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Activity13_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		WebDriver dr = new FirefoxDriver();
		WebDriverWait wait = new WebDriverWait(dr, 10);
		//Creating the JavascriptExecutor interface object by Type casting
		JavascriptExecutor js = (JavascriptExecutor)dr; 
		
		dr.get("https://www.training-support.net/selenium/lazy-loading");
		
		//Find the second card
		WebElement secondcard = dr.findElement(By.xpath("//div[@class='spaced'][2]//div[@class='image'/img]"));

		//Before Scrolling
		System.out.println("Image src1: " + secondcard.getAttribute("src"));
		
		//Scroll the card into view
		js.executeScript("window.scrollBy(0,1000)");
		
		wait.until(ExpectedConditions.not(ExpectedConditions.attributeContains(secondcard, "src", "loading")));
		//After scrolling
		System.out.println("Image src: " + secondcard.getAttribute("src"));
		
		dr.close();
		
	}

}
