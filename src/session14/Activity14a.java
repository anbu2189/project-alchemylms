package session14;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity14a {

		 WebDriver driver;
			@BeforeClass
		    public void beforeClass() {
		   	driver = new FirefoxDriver();
			//Open the browser
			driver.get("https://alchemy.hguy.co/lms");
			}
			  @Test
			    public void TestCase() throws InterruptedException {
			    //Find the navigation bar.
				  Actions actions = new Actions(driver);
				  String navbar = driver.findElement(By.xpath("//*[@id='site-navigation']")).getText();
			   		System.out.println("navigation bar is:" + navbar);
			   	WebElement menu=driver.findElement(By.xpath("//*[@id='menu-item-1507']"));
			   	actions.sendKeys(menu, Keys.ENTER).build().perform();
	WebDriverWait wait = new WebDriverWait(driver,30);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@href='#login']")));
			   	WebElement login=driver.findElement(By.xpath("//a[@href='#login']"));
			   	actions.sendKeys(login, Keys.ENTER).build().perform();
			   	 WebElement username =   driver.findElement(By.id("user_login"));
			   	 WebElement passwd =   driver.findElement(By.id("user_pass"));
			      actions.sendKeys(username,"root").perform();
			      actions.sendKeys(passwd,"pa$$w0rd").perform();
			    WebElement loginForm=driver.findElement(By.xpath("//*[@id='loginform']/p[4]"));
			    actions.sendKeys(loginForm, Keys.ENTER).build().perform();
			  }
			  //Open a course using only keyboard actions.
			  @Test(priority=1)
			    public void TestCase1() {
				  Actions actions = new Actions(driver);
				 	actions.sendKeys(Keys.PAGE_DOWN).build().perform();
				  			  WebDriverWait wait = new WebDriverWait(driver,10);
				  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='post-69']")));
				  	WebElement course=driver.findElement(By.xpath("//*[@id='post-69']"));
		actions.sendKeys(course, Keys.ENTER).build().perform();
			  }
}
